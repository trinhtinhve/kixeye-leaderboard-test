'use strict';

/* global $ io Cookies */

$(document).ready(function () {
  var socket = io(); // initialise socket.io connection

  socket.send({event: 'getAllUserScore', params: {}});


  socket.on('message', function(msg) {
    switch (msg.event) {
      case 'getAllUserScore':
        handleGetAllUserScore(msg.params);
        break;
      case 'updateUserScore':
        handleUpdateUserScore(msg.params);
        break;
    }
  });

  function handleGetAllUserScore(userScores) {
    userScores.forEach(function(userScore) {
      renderUserScore(userScore.username, userScore.score);
    });
  }

  function handleUpdateUserScore(userScore) {
    renderUserScore(userScore.username, userScore.score);
  }

  function renderUserScore(username, score) {
    var htmlText = '<li>';
    htmlText += 'username: ' + username + ' --- score: ' + score;
    htmlText += '</li>';

    $('#userScores').append(htmlText);
  }

});
