# KIXEYE Leaderboard Service Challenge

## Description

Implement a leaderboard service that could be consumed by a game client.  This service will be run on a remote server from the client.

## Requirements

Please implement the following API's:

* HTTP REST
* Persistent Socket Connection

## User Stories:

1. As a user I should be able to add/update a username and a score.
1. As a user I should be able to receive updates pushed to my screen when another user adds/updates their score.
1. As an administrator I should be able to see how many users updated their score in a time window.
1. As an administrator I should be able to see how many times a user updated their score.
1. As an administrator I should be able to delete a username and score.

## Assumptions

Please state any reasonable assumptions regarding your implementation (around any supporting services or infrastructure) in a README.md

## Deliverables

Submit your work in one of the following formats:

1. Tar/Zip/Rar'ed application
2. Link to an open source repository (bitbucket, github, etc)

Please include a README.md with documentation on how to compile and run the application.

## Additional Details

The goals of this challenge can be found in the [goals section](goals/developer.md).  It includes how we evaluate submissions.