# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Overview ###

* The code test by KIXEYE: Leaderboard Service Challenge.

### Set it up and run ###

* Configurations:
    Please take a look in Development.js. You can edit values which are host:port of server and host:port of redis cache.

* Dependencies:
    * Nodejs 8.x: https://nodejs.org/en/
    * Redis for windows: https://github.com/MicrosoftArchive/redis/releases. If you don't use windows check link: https://redis.io/download.
    
* Deployment instructions
    * Clone code at: https://trinhtinhve@bitbucket.org/trinhtinhve/kixeye-leaderboard-test.git
    * Open the project and type: npm install.
    * After dependencies installed and Redis started, type: npm start.
    
* User manuals
    * After the app started, just open the link in your browser: http://localhost:3000/docs/. This link describes APIs of project.
    * There are APIs for User and Admin.
    * Use API /user/sign-up-local to create a user:
        * click Try it out -> type a username -> click Execute.
    * Server responses a result with a token.
    * Click Authorize button -> copy and paste that token into the text field -> click Authorize -> close popup.
    * After Authenticated for user, we can use APIs for user. If authenticated for admin, we can use APIs for admin. 
    * Use API /user/update-score to add/update score.
    * Open link http://localhost:3000/leader-board/. You can see user's score is updated immediately when have users update their score.
    * And the other APIs is used like above steps.
    * You can see instruction screenshots at: https://docs.google.com/document/d/15qzVsK_i8R4WFVXKbVS10LDPD0cj5UQKcB3RupMxzYM/edit
    
### Assumptions ###

* The project have APIs for User and Admin, and WebSocket. So, we can split them to 3 projects, because it's easier for scaling our system.
* API signs up a admin /admin/sign-up that just for testing, we can not sign up a admin by admin tool.
* I used Redis for Database. We can use other DB as MySql, MongolDB,....
