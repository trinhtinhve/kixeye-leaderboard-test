const caching = require('./../services/Caching');
const cachingKeys = require('./../contansts/CachingKeys');
const dateUtils = require('./../utils/DateUtils');

module.exports = {
  logUserUpdateScore: async (username, score, scoreOffset) => {
    let scoreObject = {time: dateUtils.formatDateTime(), username, score, scoreOffset};
    try {
      return await caching.lpush(cachingKeys.LOG_USER_UPDATE_SCORE_RKEY, 0, JSON.stringify(scoreObject));
    } catch (err) {
      throw err;
    }
  },

  getLogUserUpdatedScore: async () => {
    try {
      const updatedScoresLog = await caching.lrange(cachingKeys.LOG_USER_UPDATE_SCORE_RKEY);
      if (updatedScoresLog) {
        return updatedScoresLog;
      } else {
        return null;
      }
    } catch(err) {
      throw err;
    }
  },

  getAllUserModelFromCache: async () => {
    try {
      return await caching.hgetall(cachingKeys.USERS_HKEY);
    } catch (err) {
      throw err;
    }
  },

  getUserModelFromCacheByUsername: async (username) => {
    try {
      const userModel = await caching.hget(cachingKeys.USERS_HKEY, username);
      if (userModel) {
        return JSON.parse(userModel);
      } else {
        return null;
      }
    } catch(err) {
      throw err;
    }
  },

  setUserModelToCache: async (userModel) => {
    try {
      return await caching.hset(cachingKeys.USERS_HKEY, userModel.username, JSON.stringify(userModel));
    } catch(err) {
      throw err;
    }
  },

  delUserModelFromCacheByUsername: async (username) => {
    try {
      return await caching.hdel(cachingKeys.USERS_HKEY, username);
    } catch(err) {
      throw err;
    }
  },

};
