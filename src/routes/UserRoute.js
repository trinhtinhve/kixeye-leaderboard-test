const Joi = require('joi');
const controller = require('./../controllers/UserController');
const userModel = require('./../models/UserModel');

const errorCodeModel = require('./../models/ErrorCodeModel');
const pluginResponsesDef = require('./../models/PluginResponseModel');

const routes = [
  {
    method: 'POST',
    path: '/user/sign-up-local',
    config: {
      tags: ['api', 'user'],
      description: 'User sign up by local - add username',
      auth: false,
      handler: controller.signUpLocal,
      validate: {
        payload: {
          username: Joi.string().required().min(3).max(50).description('Username')
        }
      },
      response: {
        schema: Joi.object({
          ...errorCodeModel,
          userModel: userModel.getModel()
        }).label('UserSignUpLocalResult'),
      },
      plugins: pluginResponsesDef
    }
  },

  {
    method: 'POST',
    path: '/user/login-local',
    config: {
      tags: ['api', 'user'],
      description: 'User login by local',
      auth: false,
      handler: controller.loginLocal,
      validate: {
        payload: {
          username: Joi.string().required().min(3).max(50).description('Username'),
        }
      },
      response: {
        schema: Joi.object({
          ...errorCodeModel,
          userModel: userModel.getModel()
        }).label('UserLoginLocalResult'),
      },
      plugins: pluginResponsesDef
    }
  },

  {
    method: 'POST',
    path: '/user/logout',
    config: {
      tags: ['api', 'user'],
      description: 'User logout by local',
      handler: controller.logout,
      response: {
        schema: Joi.object({
          ...errorCodeModel
        }).label('UserLogoutLocalResult'),
      },
      plugins: pluginResponsesDef
    }
  },

  {
    method: 'POST',
    path: '/user/update-score',
    config: {
      tags: ['api', 'user'],
      description: 'Update users score - add/subtract score',
      handler: controller.updateScore,
      validate: {
        payload: {
          scoreOffset: Joi.number().integer().description('Score will be added/subtracted')
        }
      },
      response: {
        schema: Joi.object({
          ...errorCodeModel,
          currentScore: Joi.number().integer().description('Score after updated')
        }).label('UpdateScoreResult'),
      },
      plugins: pluginResponsesDef
    }
  },

];

module.exports = routes;
