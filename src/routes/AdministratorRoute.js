const Joi = require('joi');
const controller = require('./../controllers/AdministratorController');
const adminModel = require('./../models/AdministratorModel');
const userModel = require('./../models/UserModel');

const errorCodeModel = require('./../models/ErrorCodeModel');
const pluginResponsesDef = require('./../models/PluginResponseModel');

const routes = [
  {
    method: 'POST',
    path: '/admin/sign-up',
    config: {
      tags: ['api', 'administrator'],
      description: 'Admin sign up - Just only for test',
      auth: false,
      handler: controller.signUp,
      validate: {
        payload: {
          adminName: Joi.string().required().min(3).max(50).description('Admin name'),
          pass: Joi.string().required().min(1).max(255).description('Password'),
        }
      },
      response: {
        schema: Joi.object({
          ...errorCodeModel,
          adminModel: adminModel.getModel()
        }).label('AdminSignUpResult'),
      },
      plugins: pluginResponsesDef
    }
  },

  {
    method: 'POST',
    path: '/admin/login',
    config: {
      tags: ['api', 'administrator'],
      description: 'Admin login',
      auth: false,
      handler: controller.login,
      validate: {
        payload: {
          adminName: Joi.string().required().min(3).max(50).description('Admin name'),
          adminPass: Joi.string().required().min(1).max(255).description('Password'),
        }
      },
      response: {
        schema: Joi.object({
          ...errorCodeModel,
          adminModel: adminModel.getModel()
        }).label('AdminLoginResult'),
      },
      plugins: pluginResponsesDef
    }
  },

  {
    method: 'POST',
    path: '/admin/logout',
    config: {
      tags: ['api', 'administrator'],
      description: 'Admin logout',
      handler: controller.logout,
      response: {
        schema: Joi.object({
          ...errorCodeModel
        }).label('AdminLogoutResult'),
      },
      plugins: pluginResponsesDef
    }
  },

  {
    method: 'GET',
    path: '/admin/get-list-user-updated-scores',
    config: {
      tags: ['api', 'administrator'],
      description: 'Get log users updated their score',
      handler: controller.getUsersUpdatedScore,
      validate: {
        query: {
          offset: Joi.number().integer().min(1).max(100).description('offset for every get updated scores log for pagination').example(100)
        }
      },
      response: {
        schema: Joi.array().items(
          Joi.object({
            time: Joi.string().description('time updates score (yyyy-MM-dd HH-mm-ss)'),
            username: Joi.string().description('Username'),
            score: Joi.number().integer().description('Current score'),
            scoreOffset: Joi.number().integer().description('Score is added/subtracted'),
          }).label('UserUpdatedScoreModel')
        ).label('LogUserUpdatedScore'),
      },
      plugins: pluginResponsesDef
    }
  },

  {
    method: 'GET',
    path: '/admin/get-update-score-count',
    config: {
      tags: ['api', 'administrator'],
      description: 'Get times users updated their score',
      handler: controller.getUpdateScoreCount,
      validate: {
        query: {
          username: Joi.string().required().description('Username'),
        }
      },
      response: {
        schema: Joi.object({
          ...errorCodeModel,
          username: Joi.string().description('Username'),
          updateScoreCount: Joi.number().integer().description('Times users updated their score'),
        }).label('UserUpdatedScoreCountModel')
      },
      plugins: pluginResponsesDef
    }
  },

  {
    method: 'POST',
    path: '/admin/delete-username',
    config: {
      tags: ['api', 'administrator'],
      description: 'Delete a username',
      handler: controller.deleteUsername,
      validate: {
        payload: {
          username: Joi.string().required().description('Username'),
        }
      },
      response: {
        schema: Joi.object({
          ...errorCodeModel,
          username: Joi.string().description('Username deleted'),
        }).label('DeleteUsernameResult'),
      },
      plugins: pluginResponsesDef
    }
  },

  {
    method: 'POST',
    path: '/admin/delete-score',
    config: {
      tags: ['api', 'administrator'],
      description: 'Delete score of username',
      handler: controller.deleteScore,
      validate: {
        payload: {
          username: Joi.string().required().description('Username'),
        }
      },
      response: {
        schema: Joi.object({
          ...errorCodeModel,
          userModel: userModel.getModel()
        }).label('DeleteScoreResult'),
      },
      plugins: pluginResponsesDef
    }
  },

];

module.exports = routes;
