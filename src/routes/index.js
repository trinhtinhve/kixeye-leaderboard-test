module.exports = {
  routes: [
    ...require('./UserRoute'),
    ...require('./AdministratorRoute'),
  ]
};
