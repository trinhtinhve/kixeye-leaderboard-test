const Boom = require('boom');

const helper = require('./../helpers/UserHelper');
const model = require('./../models/UserModel');

const webSocket = require('./../services/WebSocket');
const eventTypes = require('./../socketHandlers/EventTypes');
const logger = require('../utils/Logger');
const errorCodes = require('./../contansts/ErrorCodes');

const { generateToken } = require('../utils/JwtAuth');

module.exports = {
  signUpLocal: async (request, reply) => {
    const { username } = request.payload;

    try {
      let userModel = await helper.getUserModelFromCacheByUsername(username);

      if (userModel) {
        return reply({
          ...errorCodes.SIGN_UP_LOCAL.USER_EXIST
        });
      }

      userModel = model.create(username);

      userModel.token = generateToken(userModel.username);
      await helper.setUserModelToCache(userModel);

      return reply({
        ...errorCodes.SUCCESS,
        userModel
      });

    } catch (err) {
      logger.error(err.message, err);
      return reply(Boom.badImplementation());
    }
  },

  loginLocal: async (request, reply) => {
    const { username } = request.payload;

    try {
      let userModel = await helper.getUserModelFromCacheByUsername(username);

      if (!userModel) {
        return reply({
          ...errorCodes.LOGIN_LOCAL.USERNAME_INVALID
        });
      }

      userModel.token = generateToken(userModel.username);
      await helper.setUserModelToCache(userModel);

      return reply({
        ...errorCodes.SUCCESS,
        userModel
      });

    } catch (err) {
      logger.error(err.message, err);
      return reply(Boom.badImplementation());
    }
  },

  logout: async (request, reply) => {
    const { username } = request.auth.credentials;

    try {
      await helper.delUserModelFromCacheByUsername(username);

      reply({
        ...errorCodes.SUCCESS
      });

    } catch (err) {
      logger.error(err.message, err);
      return reply(Boom.badImplementation());
    }
  },

  updateScore: async (request, reply) => {
    const { username } = request.auth.credentials;
    const { scoreOffset } = request.payload;

    try {
      let userModel = await helper.getUserModelFromCacheByUsername(username);

      userModel.score += scoreOffset;
      if (userModel.score <= 0) userModel.score = 0;
      userModel.updateScoreCount++;

      await helper.setUserModelToCache(userModel);
      await helper.logUserUpdateScore(userModel.username, userModel.score, scoreOffset);
      webSocket.publish({ event: eventTypes.updateUserScore, params: { username, score: userModel.score } });

      reply({
        ...errorCodes.SUCCESS,
        currentScore: userModel.score
      });

    } catch (err) {
      logger.error(err.message, err);
      return reply(Boom.badImplementation());
    }
  },

};
