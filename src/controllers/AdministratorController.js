const Boom = require('boom');
const bcrypt = require('bcryptjs');

const helper = require('./../helpers/AdministratorHelper');
const userHelper = require('./../helpers/UserHelper');

const model = require('./../models/AdministratorModel');

const utils = require('./../utils/Utils');
const logger = require('./../utils/Logger');
const errorCodes = require('./../contansts/ErrorCodes');

const { generateToken } = require('../utils/JwtAuth');

module.exports = {
  signUp: async (request, reply) => {
    const { adminName, pass } = request.payload;

    try {
      let adminModel = await helper.getAdminModelFromCacheByAdminName(adminName);
      if (adminModel) {
        return reply({
          ...errorCodes.SIGN_UP_LOCAL.ADMIN_EXIST
        });
      }

      const passHashed = await utils.hash(pass);

      adminModel = model.create(adminName, passHashed);
      adminModel.token = await generateToken(adminModel.adminName);

      await helper.setAdminModelToCache(adminModel);

      delete adminModel.pass;
      return reply({
        ...errorCodes.SUCCESS,
        adminModel
      });

    } catch (err) {
      logger.error(err.message, err);
      return reply(Boom.badImplementation());
    }
  },

  login: async (request, reply) => {
    const { adminName, adminPass } = request.payload;

    try {
      let adminModel = await helper.getAdminModelFromCacheByAdminName(adminName);

      if (!adminModel) {
        return reply({
          ...errorCodes.LOGIN_LOCAL.ADMIN_NAME_INVALID
        });
      }

      const passCompare = await bcrypt.compare(adminPass, adminModel.pass);
      if (!passCompare) {
        return reply({
          ...errorCodes.LOGIN_LOCAL.ADMIN_PASS_INVALID
        });
      }

      adminModel.token = generateToken(adminModel.adminName);
      await helper.setAdminModelToCache(adminModel);

      delete adminModel.pass;
      return reply({
        ...errorCodes.SUCCESS,
        adminModel
      });

    } catch (err) {
      logger.error(err.message, err);
      return reply(Boom.badImplementation());
    }
  },

  logout: async (request, reply) => {
    const { username: adminName } = request.auth.credentials;

    try {
      await helper.delAdminModelFromCacheByAdminName(adminName);

      reply({
        ...errorCodes.SUCCESS
      });

    } catch (err) {
      logger.error(err.message, err);
      return reply(Boom.badImplementation());
    }
  },

  getUsersUpdatedScore: async (request, reply) => {
    try {
      const usersUpdatedScore = await userHelper.getLogUserUpdatedScore();
      reply(usersUpdatedScore);

    } catch (err) {
      logger.error(err.message, err);
      return reply(Boom.badImplementation());
    }
  },

  getUpdateScoreCount: async (request, reply) => {
    const { username } = request.query;

    try {
      const userModel = await userHelper.getUserModelFromCacheByUsername(username);
      if (!userModel) {
        return reply({
          ...errorCodes.ADMIN.USERNAME_INVALID
        });
      }

      return reply({
        ...errorCodes.SUCCESS,
        username: userModel.username,
        updateScoreCount: userModel.updateScoreCount
      });

    } catch (err) {
      logger.error(err.message, err);
      return reply(Boom.badImplementation());
    }
  },

  deleteUsername: async (request, reply) => {
    const { username } = request.payload;

    try {
      const success = await userHelper.delUserModelFromCacheByUsername(username);
      if (!success) {
        return reply({
          ...errorCodes.ADMIN.DELETE_USERNAME_FAILED,
          username
        });
      }

      return reply({
        ...errorCodes.SUCCESS,
        username
      });

    } catch (err) {
      logger.error(err.message, err);
      return reply(Boom.badImplementation());
    }
  },

  deleteScore: async (request, reply) => {
    const { username } = request.payload;

    try {
      let userModel = await userHelper.getUserModelFromCacheByUsername(username);
      if (!userModel) {
        return reply({
          ...errorCodes.ADMIN.USERNAME_INVALID
        });
      }

      userModel.score = 0;
      userModel.updateScoreCount = 0;
      await userHelper.setUserModelToCache(userModel);

      return reply({
        ...errorCodes.SUCCESS,
        userModel
      });

    } catch (err) {
      logger.error(err.message, err);
      return reply(Boom.badImplementation());
    }
  },

};
