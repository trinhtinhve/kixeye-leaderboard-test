const Jwt = require('jsonwebtoken');
const { jwtKey, jwtExpiresIn } = require('../config/index');
const userHelper = require('./../helpers/UserHelper');
const adminHelper = require('./../helpers/AdministratorHelper');

const validateToken = async (tokenDecoded, request, callback) => {
  const { authorization: token } = request.headers;

  const isAdminPath = request.path.indexOf('admin') >= 0;
  let model;

  if (isAdminPath) {
    model = await adminHelper.getAdminModelFromCacheByAdminName(tokenDecoded.username);
  } else {
    model = await userHelper.getUserModelFromCacheByUsername(tokenDecoded.username);
  }

  if (!model || model.token !== token) {
    return callback(null, false);
  }

  return callback(null, true, tokenDecoded);
};

module.exports = {
  apply: (server) => {
    server.auth.strategy('jwt', 'jwt', {
      key: jwtKey,
      validateFunc: validateToken,
      verifyOptions: { algorithms: ['HS256'] }
    });

    server.auth.default('jwt');
  },

  generateToken: (username) => {
    return Jwt.sign({ username }, jwtKey, { algorithm: 'HS256', expiresIn: jwtExpiresIn });
  }
};
