const logger = require('./../utils/Logger');

class AbsHandler {
  constructor(socket) {
    this.socket = socket;
  }

  _logInfo(msg) {
    logger.info(msg);
  }

  _logError(msg, err = null) {
    logger.error(msg, err)
  }

  _send(msg) {
    this.socket.send(msg);
  }

  _checkParams(params) {
    throw new Error('Not implement checkParams funciton yet');
  }

  _handleEvent(params) {
    throw new Error('Not implement handle function yet');
  }

  handleEvent(params) {
    if (this._checkParams(params)) {
      this._handleEvent(params);
    }
  }
}

module.exports = AbsHandler;
