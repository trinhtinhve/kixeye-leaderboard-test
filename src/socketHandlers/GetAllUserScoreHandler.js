const AbsHandler = require('./AbsHandler');
const eventTypes = require('./EventTypes');

const userHelper = require('./../helpers/UserHelper');

class GetAllUserModelsHandler extends AbsHandler {
  _checkParams(params) {
    return true;
  }

  _handleEvent(params) {
    userHelper.getAllUserModelFromCache().then((userModels) => {
      let userScores = [];

      if (userModels) {
        Object.values(userModels).forEach((userModel) => {
          userModel = JSON.parse(userModel);
          userScores.push({ username: userModel.username, score: userModel.score });
        });
      }

      this._send({ event: eventTypes.getAllUserScore, params: userScores });

    }).catch((err) => {
      super._logError(err.message, err);
    });
  }
}

module.exports = GetAllUserModelsHandler;
