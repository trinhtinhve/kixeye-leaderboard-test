const Joi = require('joi');

module.exports = {
  errorCode: Joi.number().integer().description('Error Code'),
  errorMessage: Joi.string().description('Error Message'),
};
