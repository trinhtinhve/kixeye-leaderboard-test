const Joi = require('joi');

module.exports = {
  create: (adminName, pass) => {
    return {
      adminName,
      pass,
      isActive: true,
      token: ''
    };
  },

  getModel: () => {
    return Joi.object({
      adminName: Joi.string().description('Admin name'),
      isActive: Joi.boolean().description('Is active'),
      token: Joi.string().description('Access Token is use to the other actions'),
    }).label('UserModel');
  }
};
