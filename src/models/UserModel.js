const Joi = require('joi');

module.exports = {
  create: (username) => {
    return {
      username,
      score: 0,
      updateScoreCount: 0,
      token: ''
    };
  },

  getModel: () => {
    return Joi.object({
      username: Joi.string().description('Username'),
      score: Joi.number().integer().description('Score of user'),
      updateScoreCount: Joi.number().integer().description('Times user updated score'),
      token: Joi.string().description('Access Token is use to the other actions'),
    }).label('UserModel');
  }
};
