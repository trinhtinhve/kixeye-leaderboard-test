const redis = require('redis');
const handleError = require('hapi-error').handleError;
const SocketIO = require('socket.io');
const logger = require('./../utils/Logger');

const { port, host } = require('./../config').cachingConfig;

const eventTypes = require('./../socketHandlers/EventTypes');
const GetAllUserScoreHandler = require('../socketHandlers/GetAllUserScoreHandler');

class WebSocket {
  static _handleEvent(msg, socket) {
    let handler;

    switch (msg.event) {
      case eventTypes.getAllUserScore:
        handler = new GetAllUserScoreHandler(socket);
        break;
      default:
        return;
    }

    handler.handleEvent(msg.params);
    handler = null;
  }

  constructor() {
    this.pub = null;
    this.sub = null;
    this.io = null;
    this.isReady = false;
  }

  _onConnection(socket) {
    socket.on('message', (msg) => {
      WebSocket._handleEvent(msg, socket);
    });

    socket.on('error', function (error) {
      handleError(error, error.stack);
    });
  }

  init(listener, initSuccess) {
    this.pub = redis.createClient(port, host);
    this.sub = redis.createClient(port, host);

    const self = this;

    self.pub.on('ready', () => {
      self.sub.on('ready', () => {
        logger.info('PUB - SUB Ready');

        self.sub.subscribe('pubsub');

        self.io = SocketIO.listen(listener);
        self.io.on('connection', self._onConnection.bind(self));

        self.sub.on('message', function (channel, msg) {
          self.send(JSON.parse(msg));
        });

        return setTimeout(() => {
          self.isReady = true;

          logger.info('WebSocket ready');

          return initSuccess();
        }, 300);
      });
    });
  }

  publish(msg) {
    if (this.isReady) {
      this.pub.publish('pubsub', JSON.stringify(msg));
    }
  }

  send(msg) {
    if (this.isReady) {
      this.io.send(msg);
    }
  }

}

module.exports = new WebSocket();
