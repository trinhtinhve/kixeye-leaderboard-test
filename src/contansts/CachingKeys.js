const cachingKeys = {
  USERS_HKEY: 'users',                      // <'users', username, userModel>
  ADMINS_HKEY: 'admins',                    // <'admins', adminName, adminModel>

  LOG_USER_UPDATE_SCORE_RKEY: 'log_uus'     // <'log_uus', [ {time, username, score, scoreOffset},... ]>
};

module.exports = cachingKeys;
