module.exports = {
  SUCCESS: {
    errorCode: 0,
    errorMessage: 'Success'
  },

  SIGN_UP_LOCAL: {
    USER_EXIST: {
      errorCode: 1001,
      errorMessage: 'Username is exist'
    },
    ADMIN_EXIST: {
      errorCode: 1002,
      errorMessage: 'Admin is exist'
    }
  },

  LOGIN_LOCAL: {
    USERNAME_INVALID: {
      errorCode: 1011,
      errorMessage: 'Username is invalid'
    },
    ADMIN_NAME_INVALID: {
      errorCode: 1012,
      errorMessage: 'Admin name is invalid'
    },
    ADMIN_PASS_INVALID: {
      errorCode: 1013,
      errorMessage: 'Admin pass is invalid'
    }
  },

  ADMIN: {
    USERNAME_INVALID: {
      errorCode: 1101,
      errorMessage: 'Username is invalid'
    },
    DELETE_USERNAME_FAILED: {
      errorCode: 1102,
      errorMessage: 'Delete username failed'
    }
  },

};
